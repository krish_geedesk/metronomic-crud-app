<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<base href="<?php echo base_url();?>">
	<title>Metronomic Crud App</title>
	<meta name="description" content="Pintasticapi is a simple REST API that provides you the city and state details of Indian pincodes">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="icon" type="image/png" sizes="96x96" href="static/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="static/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="static/images/favicon/favicon-16x16.png">
    <script src="static/js/lib/vue.min.js"></script>
    <script src="static/js/lib/axios.min.js"></script>
    <script src="static/js/lib/lodash.min.js"></script>
    <script src="static/js/lib/sweetalert2.all.min.js"></script>
</head>
<body>

<div id="container">
	<h1 align="center">Metronomic Notifications</h1>
    <br><br>
    <div id="notification-app">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div v-if="notificationsCount > 0">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Title</th>
                            <th scope="col">Created At</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item, index) in notifications">
                                <td>{{item.title}}</td>
                                <td>{{formatDate(item.created_at)}}</td>
                                <td>
                                    <button class="btn btn-xs btn-primary" data-bs-toggle="modal" data-bs-target="#notificationDetailsModal" @click="loadNotificationData(item.id, item.title)">View</button>
                                    <button class="btn btn-danger btn-xs" @click="deleteNotification(item.id)">Delete</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div v-else>
                    <p align="center">No notifications available</p>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="notificationDetailsModal" tabindex="-1" aria-labelledby="notificationDetailsModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{notificationTitle}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        {{notificationBody}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="static/js/app/notifications.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>