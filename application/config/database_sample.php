<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	
	'hostname' => 'localhost',
	
	//Production
	'username' => 'supboysalways',
	'password' => 'S3m4Fi9ur3@2018bus1n3ss1430933344313233ddsd@1',

	//Localhost
	// 'username' => 'root',
	// 'password' => '11Test11$#@',
	
	'database' => 'pintasticdb',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
