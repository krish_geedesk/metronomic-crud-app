<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Notification API
 * @author Krishnan <skrishnan784@gmail.com>
 */

require(APPPATH.'/libraries/REST_Controller.php');

class Notifications_api extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get all notifications
     * @author Krishnan <skrishnan784@gmail.com>
     *
     * @return array
     */
    public function notifications_get()
    {
        $notifications = $this->notifications_model->get_notifications();

        if (!empty($notifications) && count($notifications) > 0) {
            $this->response($notifications, 200);
        }
        else {
            $this->response(array('status' => 'failed', 'message' => 'No notifications available'), 200);
        }
    }


    /**
     * Delete a given notification
     *
     * @author Krishnan <skrishnan784@gmail.com>
     *
     * @param notification_id INT
     *
     * @return boolean
     */
    public function delete_notification_post()
    {
        $notification_id = $this->post('notification_id');
        $notification_id = trim($notification_id);
        $notification_id = $this->security->xss_clean($notification_id);
        $notification_id = (int)$notification_id;

        if ($notification_id !== '' && $notification_id !== null && is_numeric($notification_id)) {
            $delete_notification = $this->notifications_model->delete_notification($notification_id);

            if ($delete_notification) {
                $this->response(array('status' => 'success', 'message' => 'Notification deleted successfully'), 200);
            }
            else {
                $this->response(array('status' => 'failed', 'message' => 'There was an error, please try again'), 200);
            }
        }
        else {
            $this->response(array('status' => 'failed', 'message' => 'Notification ID cannot be empty or non numeric'), 200);
        }
    }

}