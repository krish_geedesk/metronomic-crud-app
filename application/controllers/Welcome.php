<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This controller is used to load views
 *
 * @author Krishnan <skrishnan784@gmail.com>
 */

class Welcome extends CI_Controller {

	
	public function index()
	{
		$this->load->view('welcome_view');
	}

    /**
     * Method to create new notifications for testing
     */
    public function create_notifications()
    {
        for ($i = 1; $i <= 50; $i++) {
            $notification_title = 'Notification '.$i;
            $notification_body = 'Notification Body '.$i;
            $this->notifications_model->create_notifications($notification_title, $notification_body);
        }
    }
}
