<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Notification Model
 * @author Krishnan <skrishnan784@gmail.com>
 */

class Notifications_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * @return array|array[]
     */
    public function get_notifications()
    {
        $this->db->select('id, title, created_at, body');
        $this->db->from('notifications');
        $this->db->order_by('id', 'desc');;
        return $this->db->get()->result_array();
    }


    /**
     * @param $notification_id
     * @return false|mixed|string
     */
    public function delete_notification($notification_id)
    {
        $this->db->where('id', $notification_id);
        return $this->db->delete('notifications');
    }


    /**
     * @param $notification_title
     * @param $notification_body
     * @return boolean
     */
    public function create_notifications($notification_title, $notification_body)
    {
        $data = array(
          'title' => $notification_title,
            'body' => $notification_body,
            'created_at' => date('Y-m-d H:i:s')
        );
        $this->db->insert('notifications', $data);
    }

}