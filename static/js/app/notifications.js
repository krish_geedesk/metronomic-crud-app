var app = new Vue({
    el: '#notification-app',
    data: {
        notificationsCount: 0,
        notifications: [],
        notificationId: '',
        notificationTitle: '',
        notificationBody: ''
    },
    mounted: function()
    {
        this.getNotifications()
    },
    methods: {
        getNotifications: function() {
            var self = this
            var url = 'api/v1/notifications'
            axios.get(url)
                .then(function (response) {
                    if (response.data.status != 'failed') {
                        self.notifications = response.data
                        self.notificationsCount = self.notifications.length
                    }
                    else {
                        self.notificationsCount = 0
                        self.notifications = []
                    }
                })
                .catch(function (error) {
                    self.notificationsCount = 0
                    self.notifications = []
                })
        },
        formatDate: function(date) {
            var formatedDate = '';
            if (date !== '' && date !== null) {
                var dateTimeArr = date.split(' ');
                var onlyDate = dateTimeArr[0];
                var dateArr = onlyDate.split('-');
                formatedDate = dateArr[2] + '-' + dateArr[1] + '-' + dateArr[0] + ' ' + dateTimeArr[1]
            }
            return formatedDate;
        },
        deleteNotification: function(notificationId) {
            var self = this
            self.notificationId = notificationId
            var confirmDel = confirm("Do you really want to delete the notification?");

            if(confirmDel === true) {
                var url = 'api/v1/notifications/delete'
                axios.post(url, {
                    notification_id: self.notificationId
                })
                    .then(function (response) {
                        if (response.data.status === 'success') {
                            self.getNotifications()
                            Swal.fire('Success!',response.data.message,'success')
                        }
                        else {
                            Swal.fire('Error!',response.data.message,'error')
                        }
                    })
                    .catch(function (error) {
                        Swal.fire('Error!','There was an error, please try again','error')
                    })
            }
        },
        loadNotificationData: function(notificationId, notificationTitle) {
            var self = this
            self.notificationId = notificationId
            self.notificationTitle = notificationTitle
            var notifications = self.notifications
            self.notificationBody = ''

            for (var i = 0; i < self.notificationsCount; i++) {
                if (parseInt(notifications[i].id) === parseInt(self.notificationId)) {
                    self.notificationBody = notifications[i].body
                }
            }
        }

    } //methods end

})