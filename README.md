# README #

This is a case study as part of round two interview. This is a simple crud application

### What is this repository for? ###

This is a case study as part of round two interview. This is a simple crud application

### How do I get set up? ###

* Get access to the repo
* Clone the repo
* Create a new branch from master
* Get the DB dump from dev
* Happy coding
